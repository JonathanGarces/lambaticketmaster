package alexa;
import com.amazon.ask.Skill;
import com.amazon.ask.Skills;
import com.amazon.ask.SkillStreamHandler;


public class HelloWorldStreamHandler extends SkillStreamHandler {

	@SuppressWarnings("unchecked")
	private static Skill getSkill() {
        return Skills.standard()
                .addRequestHandlers(
                        new CancelandStopIntentHandler(),
                        new HelloWorldIntentHandler(),
                        new HelpIntentHandler(),
                        new LaunchRequestHandler(),
                        new SessionEndedRequestHandler(),
                        new MyEventsIntentHandler(),
                        new FallbackIntentHandler())
                .build();
    }

    public HelloWorldStreamHandler() {
        super(getSkill());
    }
}
