package alexa;


import java.util.Map;
import java.util.Optional;

import alexa.service.EventCaller;
import com.amazon.ask.dispatcher.request.handler.HandlerInput;
import com.amazon.ask.dispatcher.request.handler.RequestHandler;
import com.amazon.ask.model.IntentRequest;
import com.amazon.ask.model.Response;
import com.amazon.ask.model.Slot;
import com.amazon.ask.request.Predicates;

public class MyEventsIntentHandler implements RequestHandler {


    @Override
    public boolean canHandle(HandlerInput input) {
        return input.matches(Predicates.intentName("MyEventsIntent"));
    }

    @Override
    public Optional<Response> handle(HandlerInput input) {
        EventCaller eventCaller = new EventCaller();

        IntentRequest intentRequest = (IntentRequest) input.getRequestEnvelope().getRequest();

        Map<String, Slot> slots = intentRequest.getIntent().getSlots();

        String speechText = eventCaller.getEvents(slots.get("kind").getValue());
        if(speechText.length() > 800)
            speechText = speechText.substring(0,800);

        return input.getResponseBuilder()
                .withSpeech(speechText)
                .withSimpleCard("HelloWorld", speechText)
                .build();
    }

}
