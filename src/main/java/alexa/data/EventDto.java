package alexa.data;

public class EventDto {
    private String summary;
    private String start;
    private String end;
    private String location;
    private String description;

    public EventDto(String summary){
        this.summary = summary;
    }

    @Override
    public String toString() {
        return "Summary : "+summary+"\n Start-End: "+start+" to "+end+"\n Location: "+location;
    }
}
