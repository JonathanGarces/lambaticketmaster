package alexa.service;

import alexa.data.EventDto;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;


public class EventCaller {
    private String url = "http://jonathangarces-eval-prod.apigee.net/get-events/ticketMaster/getEvent?event=";
    private final CloseableHttpClient httpClient = HttpClients.createDefault();
    private String result ="";
    private JSONObject jsonObject;
    String gigs ;
    public  String getEvents(String keyword) {
        gigs  = "Hey! i have found some events.";
        keyword = keyword.replaceAll(" ","%20");
        url = url.concat(keyword);

            HttpGet request = new HttpGet(url);
        try (CloseableHttpResponse response = httpClient.execute(request)){



            HttpEntity entity = response.getEntity();
            if (entity != null) {
                // return it as a String
                result = EntityUtils.toString(entity);
                jsonObject = new JSONObject(result);

                jsonObject = jsonObject.getJSONObject("_embedded");

                JSONArray array = jsonObject.getJSONArray("events");

               for(int i = 0; i < array.length(); i++){

                   JSONObject dates = array.getJSONObject(i).getJSONObject("dates");
                   dates = dates.getJSONObject("start");
                   String start = dates.getString("localDate");
                   JSONObject embedded = array.getJSONObject(i).getJSONObject("_embedded");
                   JSONArray venue = embedded.getJSONArray("venue");
                   JSONObject resVenue  = venue.getJSONObject(0);
                   JSONObject res  = resVenue.getJSONObject("city");

                   DateTime date = DateTime.parse(start);
                   String month = date.toString("MMMMMM");
                   String splitedDate[] = start.split("-");
                   String fixedDate = month+" "+splitedDate[2]+", "+splitedDate[0];
                    result = "the date is "+fixedDate+ ",  in "+resVenue.get("name")+ ", "+res.get("name")+" city.";
                    int cont = i+1;
                    gigs = gigs.concat(" "+cont+", "+result);


               }

            }

        } catch(Exception e){
            gigs = "No events for this artist";
        }
        return gigs;

    }

}
